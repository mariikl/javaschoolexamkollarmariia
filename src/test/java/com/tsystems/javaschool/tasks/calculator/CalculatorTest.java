package com.tsystems.javaschool.tasks.calculator;

package com.tsystems.javaschool.calculator;

import java.io.IOException;



public class CalculatorTest {



	public static void main(String[] args) throws IOException {



		Calculator c = new CalculatorImpl();


		String input = "2+3";
		String expectedResult = "5";
		System.out.print("expected Result = ");
		System.out.print(expectedResult);
		System.out.print("   result =  ");
		System.out.println(c.evaluate(input));

		input = "4-6";
		expectedResult = "-2";
		System.out.print("expected Result = ");
		System.out.print(expectedResult);
		System.out.print("   result =  ");
		System.out.println(c.evaluate(input));



		input = "2*3";
		expectedResult = "6";
		System.out.print("expected Result = ");
		System.out.print(expectedResult);
		System.out.print("   result =  ");
		System.out.println(c.evaluate(input));





		input = "12/3";
		expectedResult = "4";
		System.out.print("expected Result = ");
		System.out.print(expectedResult);
		System.out.print("   result =  ");
		System.out.println(c.evaluate(input));



		input = "2+3*4";
		expectedResult = "14";
		System.out.print("expected Result = ");
		System.out.print(expectedResult);
		System.out.print("   result =  ");
		System.out.println(c.evaluate(input));




		input = "10/2-7+3*4";
		expectedResult = "10";
		System.out.print("expected Result = ");
		System.out.print(expectedResult);
		System.out.print("   result =  ");
		System.out.println(c.evaluate(input));







		input = "10/(2-7+3)*4";
		expectedResult = "-20";
		System.out.print("expected Result = ");
		System.out.print(expectedResult);
		System.out.print("   result =  ");
		System.out.println(c.evaluate(input));

		input = "22/3*3.0480";
		expectedResult = "22.352";
		System.out.print("expected Result = ");
		System.out.print(expectedResult);
		System.out.print("   result =  ");
		System.out.println(c.evaluate(input));


		input = "22/4*2.159";
		expectedResult = "11.8745";
		System.out.print("expected Result = ");
		System.out.print(expectedResult);
		System.out.print("   result =  ");
		System.out.println(c.evaluate(input));





		input = "-12)1//(";
		expectedResult = "null";
		System.out.print("expected Result = ");
		System.out.print(expectedResult);
		System.out.print("   result =  ");
		System.out.println(c.evaluate(input));


		input = "10/(5-5)";
		expectedResult = "null";
		System.out.print("expected Result = ");
		System.out.print(expectedResult);
		System.out.print("   result =  ");
		System.out.println(c.evaluate(input));



		input = "null";
		expectedResult = "null";
		System.out.print("expected Result = ");
		System.out.print(expectedResult);
		System.out.print("   result =  ");
		System.out.println(c.evaluate(input));

		input = "(12*(5-1)";
		expectedResult = "null";
		System.out.print("expected Result = ");
		System.out.print(expectedResult);
		System.out.print("   result =  ");
		System.out.println(c.evaluate(input));



		input = "5+41..1-6";
		expectedResult = "null";
		System.out.print("expected Result = ");
		System.out.print(expectedResult);
		System.out.print("   result =  ");
		System.out.println(c.evaluate(input));


		input = "5--41-6";
		expectedResult = "null";
		System.out.print("expected Result = ");
		System.out.print(expectedResult);
		System.out.print("   result =  ");
		System.out.println(c.evaluate(input));


		input = "5**41-6";
		expectedResult = "null";
		System.out.print("expected Result = ");
		System.out.print(expectedResult);
		System.out.print("   result =  ");
		System.out.println(c.evaluate(input));



		input = "5//41-6";
		expectedResult = "null";
		System.out.print("expected Result = ");
		System.out.print(expectedResult);
		System.out.print("   result =  ");
		System.out.println(c.evaluate(input));





	}
}

