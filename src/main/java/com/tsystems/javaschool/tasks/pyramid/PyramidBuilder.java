package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {


		Collections.sort(inputNumbers);
		boolean stop = false;
		for (int i = 0; i < inputNumbers.size() - 1; i++)
			if (inputNumbers.get(i) == inputNumbers.get(i + 1))
				stop = true;

		if (!stop) {
			int len = inputNumbers.size();
			int i = 0;
			while (len > 0)
			{
				i++;
				len = len - i;
			}


			int[][] multiples = new int[i][2 * i - 1]; // 2D integer array 4 строки и 2 столбца 


			int line = 0;
			int index = 0;
			for (int n = 0; n < i; n++) {
				for (int nn = 0; nn < n + 1; nn++)
				{
					if (index < inputNumbers.size())
						multiples[line][((2 * i - 1) / 2 - n) + nn * 2] = inputNumbers.get(index++);
					else
						multiples[line][((2 * i - 1) / 2 - n) + nn * 2] = 0;

				}
				line++;
			}

			
		}
		else
			System.out.println("CannotBuildPyramidException");


	}




        return multiples;
    }


}
