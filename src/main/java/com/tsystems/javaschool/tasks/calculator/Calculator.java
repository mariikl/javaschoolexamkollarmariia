package com.tsystems.javaschool.tasks.calculator;

public interface Calculator {
	Object evaluate(String s);
}
