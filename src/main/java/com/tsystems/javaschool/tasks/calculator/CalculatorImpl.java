package com.tsystems.javaschool.tasks.calculator;

import java.util.ArrayList;
import java.util.Stack;



class CalculatorImpl implements Calculator
{
	static int priority;
	static char temp;
	static Stack<Character> stack = new Stack<Character>();
	static int currentPriority;
	public CalculatorImpl()
	{}

	public Object evaluate(String s)
	{

		ArrayList <String> list = new ArrayList<String>();   //числовые значения 
		String buff = "";
		int ii = 0;


		try {
			while (ii < s.length()) {

				if (Character.isDigit(s.charAt(ii))) {
					///

					if ((ii != s.length() - 1) && ((Character.isDigit(s.charAt(ii + 1))) || (s.charAt(ii + 1) == '.')))
					{
						buff = "";
						int mid = ii;
						while ((Character.isDigit(s.charAt(mid)) || (s.charAt(mid) == '.')) && (mid != s.length() - 1))
							buff = buff + s.charAt(mid++);
						if ((Character.isDigit(s.charAt(mid))) && (mid == s.length() - 1))
							buff = buff + s.charAt(mid++);
						list.add(buff);
						ii = mid - 1;
					}
					else
					{
						buff = "";
						buff = buff + s.charAt(ii);
						list.add(buff);
					}

					ii++;
				}

				else if (s.charAt(ii) == '+' || s.charAt(ii) == '-'
					|| s.charAt(ii) == '*' || s.charAt(ii) == '/'
					|| s.charAt(ii) == '('
					|| s.charAt(ii) == ')') {


					switch (s.charAt(ii)) {

					case '/':
						priority = 2;
						break;
					case '*':
						priority = 2;
						break;
					case '+':
						priority = 1;
						break;
					case '-':
						priority = 1;
						break;
					case '(':
						priority = 0;
						break;
					}

					if (stack.empty()) {
						stack.push(s.charAt(ii));
					}


					else if (s.charAt(ii) == '(') {
						stack.push(s.charAt(ii));
					}
					else if (s.charAt(ii) == ')') {
						while (!stack.empty()) {
							if (stack.peek() == '(')
								break;
							if (!stack.empty() && stack.peek() != '(') {
								String ch = "";
								ch = ch + stack.pop();
								list.add(ch);
							}


						}stack.pop();
					}


					else if (!stack.empty()) {
						temp = stack.peek();

						switch (temp) {
						case '/':
							currentPriority = 2;
							break;
						case '*':
							currentPriority = 2;
							break;
						case '+':
							currentPriority = 1;
							break;
						case '-':
							currentPriority = 1;
							break;

						}
						if (currentPriority < priority) {
							stack.push(s.charAt(ii));
						}

						//
						else if (currentPriority >= priority) {
							while (!stack.empty()) {

								temp = stack.peek();
								switch (temp) {

								case '/':
									currentPriority = 2;
									break;
								case '*':
									currentPriority = 2;
									break;
								case '+':
									currentPriority = 1;
									break;
								case '-':
									currentPriority = 1;
									break;
								case '(':
									currentPriority = 0;
									break;
								}

								if (currentPriority >= priority) {
									String ch = "";
									ch = ch + stack.pop();
									list.add(ch);
								}
								else
									break;


							}
							stack.push(s.charAt(ii));

						}

					}
					ii++;

				}
				else { list.add("null"); break; }
			}
		}
		catch (java.util.EmptyStackException e)
		{
			list.clear();
			list.add("null");

		}



		while (!stack.empty()) {
			String ch = "";
			ch = ch + stack.pop();
			list.add(ch);

		}


		Object result = null;

		if (list.get(0) != "null") {

			int jj = 0;
			while (list.size() != 1) {

				if ((list.get(jj).equals("+")) || (list.get(jj).equals("-")) || (list.get(jj).equals("/")) || (list.get(jj).equals("*"))) {
					double fir = 0, sec = 0;
					try {
						fir = Double.parseDouble(list.get(jj - 2));
						sec = Double.parseDouble(list.get(jj - 1));
						switch (list.get(jj)) {
						case "/":
							fir = fir / sec;
							break;
						case "*":
							fir = fir * sec;
							break;
						case "+":
							fir = fir + sec;
							break;
						case "-":
							fir = fir - sec;
							break;
						}
						list.set(jj - 2, Double.toString(fir));
						list.remove(jj);
						list.remove(jj - 1);
						jj = -1;;

					}

					catch (Exception e) {
						list.clear();
						list.add("null");
						break;
					}
				}
				else if ((list.get(jj).equals("(")) || (list.get(jj).equals(")")))
				{
					list.clear();
					list.add("null");
					break;
				}
				jj++;
			}


		}
		try {
			result = Double.parseDouble(list.get(0));
			if (list.get(0) == "null")
				result = null;
			if (list.get(0) == "Infinity")
				result = null;
		}
		catch (Exception e) {}
		return result;
	}




}

